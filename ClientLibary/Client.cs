using System.Collections.Generic;
using System.Linq;
using ClientLibary.Exceptions;

namespace ClientLibary
{
    public class Client
    {
        private static Client _instance;
        public static Client Instance
        {
            get
            {
                if(_instance == null) _instance = new Client();
                return _instance;
            }
        }
        private readonly Dictionary<string,Server> _servers = new Dictionary<string, Server>();


        public IList<Server> GetAll()
        {
            return _servers.Select(x => x.Value).ToList();
        }
        
        public Server Get(string name)
        {
            return _servers[name];
        }
        /// <summary>
        /// Creates Server, throws Exception when Connection not possible
        /// </summary>
        /// <param name="name"></param>
        /// <param name="host"></param>
        /// <param name="port"></param>
        public Server AddServer(string name, string host, short port = 1337)
        {
            if(_servers.ContainsKey(name)) throw new ServerWithNameExistsException(name);
            Server server = new Server(name,host,port);
            _servers[name] = server;
            return Get(name);
        }

        public void RemoveServer(string name)
        {
            if (!_servers.ContainsKey(name)) throw new ServerNotFoundException(name);
            _servers.Remove(name);
        }
    }
}