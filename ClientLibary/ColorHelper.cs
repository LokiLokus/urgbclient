using System;
using System.Collections.Generic;
using System.Drawing;

namespace ClientLibary
{
    public class ColorHelper
    {
        
        public  static Color GetRandomColor()
        {
            Random r = new Random();
            return Color.FromArgb(r.Next(0, 256), 
                r.Next(0, 256), r.Next(0, 256));
        }
        
        public static IEnumerable<Color> GetGradients(Color start, Color end, int steps)
        {
            double stepA = (end.A - start.A) / (double)(steps - 1);
            double stepR = (end.R - start.R) / (double)(steps - 1);
            double stepG = (end.G - start.G) / (double)(steps - 1);
            double stepB = (end.B - start.B) / (double)(steps - 1);

            for (int i = 0; i < steps; i++)
            {
                yield return Color.FromArgb((int)(start.A + (stepA * i)),
                    (int)(start.R + (stepR * i)),
                        (int)(start.G + (stepG * i)),
                            (int)(start.B + (stepB * i)));
            }
        }
    }
}