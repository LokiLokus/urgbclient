using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using ClientLibary.EffectModel;
using Communication;

namespace ClientLibary
{
    public class Stripe
    {
        private readonly Communicator _communicator;
        public byte Idx { get; private set; }
        public string Name { get; private set; }
        public ushort Length { get; private set; }
        
        public Stripe(string name, byte idx,ushort length, Communicator communicator)
        {
            Idx = idx;
            Name = name;
            Length = length;
            _communicator = communicator;
        }

        public async Task SetLed(ushort idx, Color color,double opacity = 1)
        {
            await _communicator.SetPixel(idx, color,opacity);
        }

        public async Task SetStripe(Color color,double opacity = 1)
        {
            await _communicator.FillPixel(0, Length, color,opacity);
        }

        public async Task SetLeds(ushort offset, ushort length, Color color, double opacity = 1)
        {
            await _communicator.FillPixel(offset, length, color,opacity);
        }

        public async Task ExecuteEffect(Effect effect)
        {
            if(effect == null) throw new ArgumentNullException();
            foreach (var subEffect in effect.SubEffects)
            {
                await ExecuteSubEffect(subEffect);
                await Task.Delay(subEffect.ExecutionTime);
            }
        }
        
        public async Task ExecuteSubEffect(SubEffect subEffect)
        {
            if(subEffect == null) throw new ArgumentNullException();
            var tasks = subEffect.Commands.Select(ExecuteCommand).ToArray();
            await Task.WhenAll(tasks);
            await _communicator.Flush();
        }
        
        internal async Task ExecuteCommand(Command command)
        {
            if(command == null) throw new ArgumentNullException();
            Color color = command.RandomColor ? ColorHelper.GetRandomColor() : command.Color;
            await _communicator.FillPixel(command.StartIdx,command.Length,color,command.Opacity);
        }

        public async Task Flush()
        {
            await _communicator.Flush();
        }
    }
}