using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientLibary.Exceptions;
using Communication;

namespace ClientLibary
{
    public class Server
    {
        private readonly Communicator _communicator;
        private readonly Dictionary<string,Stripe> _stripes = new Dictionary<string, Stripe>();
        public readonly string Name;
        
        
        /// <summary>
        /// Creates Server, throws Exception when Connection not possible
        /// </summary>
        /// <param name="name"></param>
        /// <param name="host"></param>
        /// <param name="port"></param>
        internal Server(string name, string host, short port)
        {
            Name = name;
            _communicator = new Communicator(host,port);
        }

        public async Task SetWifi(string ssid, string password)
        {
            await _communicator.SendWifiSsid(ssid);
            await _communicator.SendWifiPassword(password);
        }

        public async Task SetWifiMode(WifiMode mode)
        {
            await _communicator.SetWifiMode(mode);
        }

        public async Task Reboot()
        {
            await _communicator.SendReboot();
        }

        public Stripe AddStripe(string name, byte idx,ushort length)
        {
            if(_stripes.ContainsKey(name) || _stripes.Any(x => x.Value.Idx == idx)) throw new StripeWithNameOrIdxExistsException(name);
            Stripe stripe = new Stripe(name,idx,length,_communicator);
            _stripes[name] = stripe;
            return stripe;
        }

        public List<Stripe> GetAll()
        {
            return _stripes.Select(x => x.Value).ToList();
        }

        public Stripe Get(string name)
        {
            return _stripes[name];
        }

        public void RemoveStripe(string name)
        {
            _stripes.Remove(name);
        }
    }
}