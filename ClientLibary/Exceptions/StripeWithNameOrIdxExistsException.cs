using System;

namespace ClientLibary.Exceptions
{
    public class StripeWithNameOrIdxExistsException:Exception
    {
        public StripeWithNameOrIdxExistsException(string name) : base(name)
        {
            
        }
    }
}