using System;

namespace ClientLibary.Exceptions
{
    public class ServerNotFoundException: Exception
    {
        public ServerNotFoundException(string name):base(name)
        {
        }
    }
}