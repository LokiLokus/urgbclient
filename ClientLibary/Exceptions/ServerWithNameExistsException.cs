using System;

namespace ClientLibary.Exceptions
{
    public class ServerWithNameExistsException: Exception
    {
        public ServerWithNameExistsException(string name):base(name)
        {
        }
    }
}