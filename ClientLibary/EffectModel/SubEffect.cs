using System;
using System.Collections.Generic;

namespace ClientLibary.EffectModel
{
    public class SubEffect
    {
        public string Name { get; set; }

        public TimeSpan ExecutionTime { get; set; }
        public Transition TransitionType { get; set; }
        public List<Command> Commands { get; set; } = new List<Command>();
    }

    public enum Transition
    {
        Cut,SlowTransition,
    }
}