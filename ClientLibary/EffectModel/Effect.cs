using System.Collections.Generic;

namespace ClientLibary.EffectModel
{
    public class Effect
    {
        public string Name { get; set; }
        public List<SubEffect> SubEffects { get; set; } = new List<SubEffect>();
    }
}