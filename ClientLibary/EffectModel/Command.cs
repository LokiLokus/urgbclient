using System.Drawing;

namespace ClientLibary.EffectModel
{
    public class Command
    {
        public ushort StartIdx { get; set; }
        public ushort Length { get; set; }

        public Color Color { get; set; }
        public double Opacity { get; set; }
        public bool RandomColor { get; set; }
    }
}