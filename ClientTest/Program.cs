﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using ClientLibary;
using ClientLibary.EffectModel;
using NAudio.Wave;

namespace ClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string host;
            if (args.Length != 0)
            {
                host = args[0];
            }
            else
            {
                Console.WriteLine("Host:");
                host = Console.ReadLine();
            }
            
            ExecuteTasks(host).Wait();
        }

        private static async Task ExecuteTasks(string host)
        {
            Console.WriteLine("Add Server");
            var server = Client.Instance.AddServer("test",host);
            //await server.Reboot();
            Console.WriteLine("Add Stripe");
            var stripe = server.AddStripe("main", 0, 188);
            Console.WriteLine("Set Color");
            await stripe.SetStripe(Color.Black,1);
            //await stripe.SetLeds(11, 66, Color.Blue, 0.1);
            //await stripe.SetStripe(Color.OrangeRed,0.4);
            Console.WriteLine("Flush");
            await stripe.Flush();
            
            List<Color> colors = new List<Color>()
            {
                Color.DarkBlue,Color.Salmon,Color.Indigo,Color.Lime,Color.DarkRed,Color.Yellow,Color.Turquoise,Color.LightGray,Color.RosyBrown,Color.Purple,Color.Green
            };
            await stripe.SetLeds(11, 66, Color.FromArgb(0, 0, 6));
            //await stripe.SetLed(ushort.Parse(Console.ReadLine()), Color.Aqua);
            
            await stripe.Flush();
            return;
            for (var i = 0; i <= 100000; i++)
            {
                

                var gradients = ColorHelper.GetGradients(colors[i%colors.Count], colors[(i+1)%colors.Count], 400).ToList();
                foreach (var gradient in gradients)
                {
                    await stripe.SetLeds(11, 66, gradient, 0.1);
                    await stripe.Flush();
                    await Task.Delay(1);
                }
                 //await stripe.ExecuteEffect(CreateEffect());
                  //await Walking(stripe);
                  /*await SetRandomLeds(stripe);
                  
                  */
            }
            return;
            for (double i = 0; i < 1; i+=0.01)
            {
                
                
                await stripe.Flush();
                await Task.Delay(20);
            }
            return;
            while (true)
            {
                //await stripe.SetLed(ushort.Parse(Console.ReadLine()), Color.Aqua);
                //await stripe.Flush();
                await stripe.ExecuteEffect(CreateEffect());
                //await Walking(stripe);
            }
            Console.WriteLine("Flushed");
            
        }

        private static Effect CreateEffect()
        {
            var effect = new Effect()
            {
                Name = "Test Effect",
            };
            var subEffect = new SubEffect()
            {
                Name = "Test Subeffect #1",
                ExecutionTime = TimeSpan.FromMilliseconds(500)
            };
            effect.SubEffects.Add(subEffect);
            subEffect.Commands.Add(new Command()
            {
                Color = Color.Blue,
                Length = 66,
                StartIdx = 11,
                Opacity = 1
            });
            subEffect.Commands.Add(new Command()
            {
                Color = Color.OrangeRed,
                Length = 64,
                StartIdx = 103,
                Opacity = 1
            });
            
            subEffect = new SubEffect()
            {
                Name = "Test Subeffect #2",
                ExecutionTime = TimeSpan.FromMilliseconds(500)
            };
            effect.SubEffects.Add(subEffect);
            subEffect.Commands.Add(new Command()
            {
                Color = Color.OrangeRed,
                Length = 66,
                StartIdx = 11,
                Opacity = 1
            });
            subEffect.Commands.Add(new Command()
            {
                Color = Color.Blue,
                Length = 64,
                StartIdx = 103,
                Opacity = 1
            });
            //effect.SubEffects.Add();
            return effect;
        }

        private static async Task Walking(Stripe stripe)
        {
            for (ushort i = 0; i < stripe.Length; i++)
            {
                await stripe.SetStripe(Color.Black,.1);
                await stripe.SetLeds(i, 5, Color.Blue);
                await stripe.SetLeds((ushort)( (i+20)%stripe.Length), 15, Color.Red);
                await stripe.SetLeds((ushort)( (i+40)%stripe.Length), 5, Color.Green);
                await stripe.SetLeds((ushort)( (i+100)%stripe.Length), 15, Color.Firebrick);
                await stripe.SetLeds((ushort)( (i+120)%stripe.Length), 5, Color.Turquoise);
                await stripe.SetLeds((ushort)( (i+60)%stripe.Length), 15, Color.Yellow);
                await stripe.SetLeds((ushort)( (i+140)%stripe.Length), 5, Color.DarkMagenta);
                await stripe.SetLeds((ushort)( (i+160)%stripe.Length), 15, Color.OrangeRed);
                await stripe.SetLeds((ushort)( (i+80)%stripe.Length), 5, Color.DarkGreen);
                
                await stripe.Flush();
                await Task.Delay(50);
            }
        }
        
        private static async Task SetRandomLeds(Stripe stripe)
        {
            for (ushort i = 0; i < stripe.Length; i++)
            {
                await stripe.SetLed(i, ColorHelper.GetRandomColor(),0.1);
                await stripe.Flush();
            }
        }
    }
}