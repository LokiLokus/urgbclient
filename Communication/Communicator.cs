using System;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Communication
{
    public class Communicator
    {
        public string Host { get; private set; }
        private readonly UdpClient _client;
        private byte[] sendBuffer = new byte[1024];
        private int bufferLength = 0;

        public Communicator(string host,short port)
        {
            Host = host;
            _client = new UdpClient(port);
            _client.Connect(Host,port);
        }

        public async Task Flush()
        {
            await SendData(new byte[]{0}, true);
        }

        public async Task SetPixel(ushort ledIdx, Color color,double opacity)
        {
            if(opacity > 1) throw new ArgumentException("Opacity must by between 0 and 1");
            var command = new byte[6];
            var idx = BitConverter.GetBytes(ledIdx);
            command[0] = 0x01;
            command[1] = idx[1];
            command[2] = idx[0];
            command[3] = (byte)(color.R * opacity);
            command[4] = (byte)(color.G * opacity);
            command[5] = (byte)(color.B * opacity);
            await SendData(command);
        }

        public async Task FillPixel(ushort offset, ushort length, Color color, double opacity)
        {
            if(opacity > 1) throw new ArgumentException("Opacity must by between 0 and 1");
            var command = new byte[8];
            var idx = BitConverter.GetBytes(offset);
            var len = BitConverter.GetBytes(length);
            command[0] = 0x02;
            command[1] = idx[1];
            command[2] = idx[0];
            command[3] = len[1];
            command[4] = len[0];
            command[5] = (byte)(color.R * opacity);
            command[6] = (byte)(color.G * opacity);
            command[7] = (byte)(color.B * opacity);
            await SendData(command);
        }

        public void SetLedCount(short count)
        {
            var command = new byte[3];
            var cnt = BitConverter.GetBytes(count);
            command[0] = 0x10;
            command[1] = cnt[0];
            command[2] = cnt[1];
        }


        public async Task SetWifiMode(WifiMode mode)
        {
            var command = new byte[2];
            command[0] = 0x20;
            command[1] = (byte) (mode == WifiMode.AP ? 0 : 1);
            await SendData(command);
        }

        public async Task SendWifiSsid(string ssid)
        {
            var command = new byte[2+ssid.Length];
            command[0] = 0x21;
            command[1] = BitConverter.GetBytes(ssid.Length).Last();
            var ssidBytes = Encoding.ASCII.GetBytes(ssid);
            for (var i = 0; i < ssidBytes.Length; i++)
            {
                command[i + 2] = ssidBytes[i];
            }
            await SendData(command);
        }
        
        public async Task SendWifiPassword(string ssid)
        {
            var command = new byte[2+ssid.Length];
            command[0] = 0x22;
            command[1] = BitConverter.GetBytes(ssid.Length).Last();
            var ssidBytes = Encoding.ASCII.GetBytes(ssid);
            for (var i = 0; i < ssidBytes.Length; i++)
            {
                command[i + 2] = ssidBytes[i];
            }
            await SendData(command);
        }

        public async Task SendReboot()
        {
            await SendData(new byte[]{0xff}, true);
        }
        
        private async Task<int> SendData(byte[] data, bool flushBuffer = false)
        {
            bool fitsInBuffer = bufferLength + data.Length < sendBuffer.Length;
            if(fitsInBuffer)
            {
                Buffer.BlockCopy(data, 0, sendBuffer, bufferLength, data.Length);
                bufferLength += data.Length;
            }

            if(flushBuffer)
            {
                int ret = await _client.SendAsync(sendBuffer, bufferLength);
                bufferLength = 0;
                return ret;
            }
            else if(!fitsInBuffer)
            {
                int ret = await _client.SendAsync(sendBuffer, bufferLength);
                Buffer.BlockCopy(data, 0, sendBuffer, 0, data.Length);
                bufferLength = data.Length;
                return ret;
            }
            else
            {
                return 0;
            }
        }
    }
}